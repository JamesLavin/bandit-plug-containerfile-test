defmodule BanditTest.Application do
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      {Bandit, plug: BanditTest.HelloWorld}
      # Starts a worker by calling: BanditTest.Worker.start_link(arg)
      # {BanditTest.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: BanditTest.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
