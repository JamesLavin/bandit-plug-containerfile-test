defmodule BanditTest.HelloWorld do
  import Plug.Conn

  def init(options) do
    options
  end

  def call(conn, _opts) do
    IO.inspect("Received #{conn.method} request to #{conn.request_path} from #{inspect conn.remote_ip}.")
    IO.inspect("Conn: #{inspect conn}")
    conn
    |> put_resp_content_type("text/plain")
    |> send_resp(200, "Hello world\n")
  end
end
